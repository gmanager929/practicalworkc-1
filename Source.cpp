#include <iostream>
#include "Helpers.h"

int main() {
    int num1 = 10;
    int num2 = 20;

   
    int result = SquareOfSum(num1, num2);

    std::cout << "Square of sum of " << num1 << " and " << num2 << " is: " << result << std::endl;

    return 0;
}